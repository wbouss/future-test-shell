<?php

#########################
# Constantes
#########################

define("FIZZBUZZ_PRIME", "FIZZBUZZ++");
define("FIZZBUZZ", "FIZZBUZZ");
define("FIZZ", "FIZZ");
define("BUZZ", "BUZZ");

#########################
# FizzBuzz
#########################

if($argv[1] == 'test') {
    if(testPrime()) {
        echo 'All tests have succeeded';
    } else {
        echo 'Test failed';
    }
} elseif ($content = fizzBuzz()) {
    echo $content;
    shell_exec("echo '$content' > file.txt");
}

/**
 * Execute the FizzBuzz function
 * @param int $end
 * @return string
 */
function fizzBuzz(int $end = 500) {

    $content = '';

    // Early return
    if($end <= 0) {
        return $content;
    }

    for ($i = 1; $i <= $end; $i++) {
        if (isPrime($i)) {
            $content .= $i." ".FIZZBUZZ_PRIME;
        }
        elseif ($i % 3 == 0 && $i % 5 == 0) {
            $content .= $i." ".FIZZBUZZ;
        } elseif ($i % 5 == 0) {
            $content .= $i." ".BUZZ;
        } elseif ($i % 3 == 0) {
            $content .= $i." ".FIZZ;
        } else {
            $content .= $i;
        }
        $content .= "\n";
    }

    return $content;
}

/**
 * Check if the number is prime
 * @param $value
 * @return bool
 */
function isPrime($value): bool {
    if ($value == 1) {
        return false;
    }

    for ($i = 2; $i <= $value/2; $i++){
        if ($value % $i == 0)
            return false;
    }

    return true;
}

/**
 * Test the FizzBuzz function
 * @return bool
 */
function testPrime(): bool
{
    return substr_count(fizzBuzz(20), FIZZBUZZ_PRIME) == 8;
}
